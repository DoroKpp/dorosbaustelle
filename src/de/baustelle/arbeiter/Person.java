package de.baustelle.arbeiter;

public class Person {
    //Eigenschaften
    String vorname;
    String name;
    String beruf;
    String firma;
    int berufserfahrung = 0;
    String position;


    //Konstruktoren
    //default-Konstruktor
    public Person() {
    }

    public Person(String vorname, String name, String beruf, String firma, int berufserfahrung, String position) {
        this.vorname = vorname;
        this.name = name;
        this.beruf = beruf;
        this.firma = firma;
        this.berufserfahrung = berufserfahrung;
        this.position = position;
    }

    //Getter / Setter
    public String getVorname() {
        return vorname;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirma() {
        return firma;
    }

    public void setFirma(String firma) {
        this.firma = firma;
    }

    public int getBerufserfahrung() {
        return berufserfahrung;
    }

    public void setBerufserfahrung(int berufserfahrung) {
        this.berufserfahrung = berufserfahrung;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getBeruf() {
        return beruf;
    }

    public void setBeruf(String beruf) {
        this.beruf = beruf;
    }

    //toString-Methode
    public String toString() {
        return  " O / \t" + getVorname() + " " + getName() + "\n" +
                " | \t\t" + "Beruf: " + getBeruf() + "\n" +
                "/ \\ \t" + "Firma: " + getFirma() + "\n" +
                "\t\t" + getBerufserfahrung() + " Jahre Berufserfahrung" + "\n" +
                "\t\tPosition: " + getPosition() + "\n";
    }
}
