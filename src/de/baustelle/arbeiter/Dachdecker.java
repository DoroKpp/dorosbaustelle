package de.baustelle.arbeiter;

public class Dachdecker extends Person {
    public Dachdecker(String vorname, String name, String beruf, String firma, int berufserfahrung, String position) {
        super(vorname, name, beruf, firma, berufserfahrung, position);
    }

    public String dachDecken(String dachstuhl) {
        StringBuilder dachpfannen = new StringBuilder(dachstuhl);
        System.out.println(this.vorname + " deckt gerade das Dach...");

        for (int i = 0; i < dachpfannen.length(); i++) {
            if (dachpfannen.charAt(i) == '-') {
                dachpfannen.setCharAt(i, 'u');
            }
        }
        return dachpfannen.toString();
    }
}
