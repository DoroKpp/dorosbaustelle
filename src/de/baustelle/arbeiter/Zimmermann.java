package de.baustelle.arbeiter;

public class Zimmermann extends Person{

    public Zimmermann(String vorname, String name, String beruf, String firma, int berufserfahrung, String position) {
        super(vorname, name, beruf, firma, berufserfahrung, position);
    }

    public String dachstuhlBauen(int breite) {
        System.out.println(this.vorname + " baut gerade den Dachstuhl...\n");
        StringBuilder dachstuhl = new StringBuilder("");
        for (int i = 0; i < 3; i++) {
            switch (i) {
                case 0:
                    dachstuhl.append("  /\\");
                    dachstuhl.append(dachstuhlSchleife(breite));
                    break;
                case 1:
                    dachstuhl.append(" /  \\");
                    dachstuhl.append(dachstuhlSchleife(breite));
                    break;
                case 2:
                    dachstuhl.append("/    \\");
                    dachstuhl.append(dachstuhlSchleife(breite));
                    break;
            }
        }

        return dachstuhl.toString();
    }

    public static String dachstuhlSchleife(int breite) {

        StringBuilder sparren = new StringBuilder("");
        for (int i = 0; i < (breite + (breite-5)/2); i++) {
            sparren.append("-\\");
        }
        sparren.append("\n");
        return sparren.toString();
    }
}
