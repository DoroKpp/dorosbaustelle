package de.baustelle.arbeiter;

public class Maurer extends Person{

    //Eigenschaften
    boolean helmAuf = false;
    boolean mörtelVorh = false;
    boolean klinkerVorh = false;
    boolean kelleVorh = false;

    //Konstruktor
    public Maurer(String vorname, String name, String beruf, String firma, int berufserfahrung, String position) {
        super(vorname, name, beruf, firma, berufserfahrung, position);
    }

    //Getter / Setter
    public boolean isHelmAuf() {
        return helmAuf;
    }

    public void setHelmAuf(boolean helmAuf) {
        this.helmAuf = helmAuf;
    }

    public boolean isMörtelVorh() {
        return mörtelVorh;
    }

    public void setMörtelVorh(boolean mörtelVorh) {
        this.mörtelVorh = mörtelVorh;
    }

    public boolean isKlinkerVorh() {
        return klinkerVorh;
    }

    public void setKlinkerVorh(boolean klinkerVorh) {
        this.klinkerVorh = klinkerVorh;
    }

    public boolean isKelleVorh() {
        return kelleVorh;
    }

    public void setKelleVorh(boolean kelleVorh) {
        this.kelleVorh = kelleVorh;
    }


    //toString-Methode
    public void helmPrüfen() {
        if (this.helmAuf) {
            System.out.println(this.vorname + " hat einen Helm auf.\n");
        } else {
            System.out.println(this.vorname + " hat noch keinen Helm auf: )-:\n" +
                    "So darf er nicht auf die Baustelle!\n");
        }
    }

    public void helmAufsetzen(){
        this.helmAuf = true;
        System.out.println("Jetzt hat " + this.vorname  +
                " einen Helm auf: (-:p\n");
    }

    public String mauern(int höhe, int breite) {
        boolean bereit = false;
        StringBuilder mauer = new StringBuilder("");

        if (!kelleVorh) {
            System.out.println(this.vorname + " hat keine Kelle.");
        }
        if (!klinkerVorh) {
            System.out.println(this.vorname + " hat keinen Klinker.");
        }
        if (!mörtelVorh) {
            System.out.println(this.vorname + " hat keinen Mörtel.");
        }
        if (kelleVorh && klinkerVorh && mörtelVorh) {
            bereit = true;
        }
        if (bereit) {
            System.out.println(this.vorname + " mauert...\n");
            for (int j = 0; j < breite; j++) {
                mauer.append(" __");
            }
            mauer.append("\n");
            for (int i = 0; i < höhe; i++) {
                for (int j = 0; j < breite; j++) {
                    if (j == 0 && i % 2 == 0) {
                        mauer.append("|__|");
                    } else if (j == 0 && i % 2 != 0) {
                        mauer.append("|_|");
                    } else {
                        mauer.append("__|");
                    }
                    if (i % 2 != 0 && j == breite - 1) {
                        mauer.append("|");
                    }
                }
                mauer.append("\n");
            }
            return mauer.toString();
        } else {
            return "So kann " + this.vorname + " nicht mauern.\n";
        }
    }


}
