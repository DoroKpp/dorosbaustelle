package de.baustelle.gebäude;

public class Haus {

    //Eigenschaften
    private String besitzer;

    private String mauer;
    private String dach;

    //Konstruktor
    public Haus(String besitzer, String mauer, String dach) {
        this.besitzer = besitzer;
        this.mauer = mauer;
        this.dach = dach;
    }

    public String getMauer() {
        return mauer;
    }

    public void setMauer(String mauer) {
        this.mauer = mauer;
    }

    public String getDach() {
        return dach;
    }

    public void setDach(String dach) {
        this.dach= dach;
    }


    public String getBesitzer() {
        return besitzer;
    }

    public void setBesitzer(String besitzer) {
        this.besitzer = besitzer;
    }

    @Override
    public String toString() {
        return "So sieht das Haus der " + this.besitzer + " aus:\n"+
                this.dach + this.mauer;
    }
}
