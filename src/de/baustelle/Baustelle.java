package de.baustelle;

import de.baustelle.gebäude.Haus;
import de.baustelle.arbeiter.Dachdecker;
import de.baustelle.arbeiter.Maurer;
import de.baustelle.arbeiter.Zimmermann;

public class Baustelle {
    public static void main(String[] args) {
        //Maurer erzeugen
        Maurer bernd = new Maurer("Bernd", "Müller", "Maurer","Maier GmbH", 20, "Polier");
        System.out.println(bernd.toString());

        System.out.println();

        //Hat er einen Helm auf?
        bernd.helmPrüfen();
        System.out.println();

        //Schnell den Helm aufsetzen:
        bernd.helmAufsetzen();
        bernd.helmPrüfen();

        //Wir brauchen noch mehr Maurer!
        Maurer peter = new Maurer("Peter", "Schmitz", "Maurer","Maier GmbH", 10, "Meister");
        Maurer kevin = new Maurer("Kevin", "Schneider", "Maurer","Maier GmbH", 2, "Azubi");

        System.out.println(peter);
        System.out.println(kevin);

        //Peter ist hat schon einen Helm auf
        peter.setHelmAuf(true);

        //Helm-Prüfung
        kevin.helmPrüfen();
        kevin.helmAufsetzen();
        peter.helmPrüfen();

        bernd.mauern(5,7);

        bernd.setKelleVorh(true);
        bernd.setMörtelVorh(true);
        bernd.setKlinkerVorh(true);

        System.out.println(bernd.mauern(3, 5));
        System.out.println(peter.mauern(6, 10));

        peter.setKelleVorh(true);
        peter.setMörtelVorh(true);
        peter.setKlinkerVorh(true);

        String blomsMauer = peter.mauern(6, 10);
        System.out.println(blomsMauer);

        //Jetzt kommt der Dachstuhl dran.
        //Wir brauchen einen (oder mehrere) Zimmermänner!
        Zimmermann stefan = new Zimmermann("Stefan", "Köhler", "Zimmermann","Rudolfs", 13, "Meister");
        System.out.println(stefan);

        String dachstuhl = stefan.dachstuhlBauen(10);
        System.out.println(dachstuhl);

        //Das Dach muss noch gedeckt werden.
        //Wir brauchen einen (oder mehrere) Dachdecker!
        Dachdecker holger = new Dachdecker("Holger", "Schäfer", "Dachdecker", "Schulz", 5, "Meister");
        System.out.println(holger);

        String blomsGedecktesDach = holger.dachDecken(dachstuhl);
        System.out.println(blomsGedecktesDach);

        System.out.println("Jetzt ist das Haus fertig!\n");

        Haus blomsHaus = new Haus("Familie Blom", blomsMauer, blomsGedecktesDach);
        System.out.println(blomsHaus);
    }
}
